const express = require('express');
const middleware = require('../Middlewares/middleware')
const { createOrder, getAllOrder, getOrderById, updateOrder, deleteOrder } = require('../Controller/orderController')

const router = express.Router();

router.use('/', middleware)

router.post('/order', createOrder)

router.get('/order', getAllOrder)

router.get('/order/:orderId', getOrderById)

router.put('/order/:orderId', updateOrder)

router.delete('/order/:orderId', deleteOrder)

module.exports = router