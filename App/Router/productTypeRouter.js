const express = require('express');
const middleware = require('../Middlewares/middleware')
const { createProductType, GetALlProductType, GetProductTypeByID, UpdateProductType, DeleteProductType } = require('../Controller/productTypeController')

const router = express.Router();

router.use('/', middleware)

router.post('/productType', createProductType)

router.get('/productType', GetALlProductType)

router.get('/productType/:productTypeId', GetProductTypeByID)

router.put('/productType/:productTypeId', UpdateProductType)

router.delete('/productType/:productTypeId', DeleteProductType)

module.exports = router