const express = require('express');
const middleware = require('../Middlewares/middleware')
const { createCustomer, getAllCustomer, getCustomerById, updateCustomer, deleteCustomer } = require('../Controller/customerController')

const router = express.Router();

router.use('/', middleware)

router.post('/customer', createCustomer)

router.get('/customer', getAllCustomer)

router.get('/customer/:customerId', getCustomerById)

router.put('/customer/:customerId', updateCustomer)

router.delete('/customer/:customerId', deleteCustomer)

module.exports = router