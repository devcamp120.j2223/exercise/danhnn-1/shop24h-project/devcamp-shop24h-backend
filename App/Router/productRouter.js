const express = require('express');
const middleware = require('../Middlewares/middleware')
const { createProduct, getAllProduct, getProductById, updateProduct, deleteProduct } = require('../Controller/productController')

const router = express.Router();

router.use('/', middleware)

router.post('/product', createProduct)

router.get('/product', getAllProduct)

router.get('/product/:productId', getProductById)

router.put('/product/:productId', updateProduct)

router.delete('/product/:productId', deleteProduct)

module.exports = router