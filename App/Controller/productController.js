const express = require('express');
const productModel = require('../Model/ProductModel');
const mongoose = require('mongoose');

const createProduct = (req, res) => {
    // thu thập dữ liệu
    let body = req.body;
    // validate
    if (!body.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Name is required"
        })
    }
    if (!body.type) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Type is required"
        })
    }
    if (!body.imageUrl) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "ImageUrl is required"
        })
    }
    if (!body.buyPrice) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "BuyPrice is required"
        })
    }
    if (!body.promotionPrice) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "PromotionPrice is required"
        })
    }
    if (!body.amount) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Amount is required"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    let productModelData = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        type: mongoose.Types.ObjectId(),
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount,
    }
    productModel.create(productModelData, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: Internal sever Error",
                message: err.message
            })
        } else {
            return res.status(201).json({
                status: "Success: Create Prodduct success",
                data: data
            })
        }
    })
}
// tạo get all  
const getAllProduct = (req, res) => {
    productModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: Internal sever Error",
                message: err.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get all Product successfully",
                data: data
            })
        }
    })
}
// tạo get by id 
const getProductById = (req, res) => {
    // lấy param 
    let productId = req.params.productId;
    //B2 : Validate
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Product Id is not valid"
        })
    }
    // B3: Thao tắc với cơ sở dữ liệu
    productModel.findById(productId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: Internal sever Error",
                message: err.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get Product by id success" + productId,
                data: data
            })
        }
    })
}
// tạo post 
const updateProduct = (req, res) => {
    // thu thập dữ liệu
    let productId = req.params.productId;
    let body = req.body;
    // validate
    // validate
    if (!body.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Name is required"
        })
    }
    if (!body.type) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Type is required"
        })
    }
    if (!body.imageUrl) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "ImageUrl is required"
        })
    }
    if (!body.buyPrice) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "BuyPrice is required"
        })
    }
    if (!body.promotionPrice) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "PromotionPrice is required"
        })
    }
    if (!body.amount) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Mount is required"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    let productUpdate = {
        name: body.name,
        type: mongoose.Types.ObjectId(),
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount,
    }
    productModel.findByIdAndUpdate(productId, productUpdate, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: Internal sever error",
                message: err.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Update product type success",
                data: data
            })
        }
    })
}
// tạo post 
const deleteProduct = (req, res) => {
    // B1: thu thập dữ liệu
    let productId = req.params.productId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "ProductType Id is not valid"
        })
    }

    //B3: Thao tắc với cơ sở dữ liệu
    productModel.findByIdAndDelete(productId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: Internal sever error",
                message: err.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Update product success" + productId + " Success ",
            })
        }
    })
}

module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProduct,
    deleteProduct
}
