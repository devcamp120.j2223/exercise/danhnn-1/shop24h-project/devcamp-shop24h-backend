const express = require('express');
const orderModel = require('../Model/OrderModel');
const mongoose = require('mongoose');

const createOrder = (req, res) => {
    // thu thập dữ liệu
    let body = req.body;
    // b3 sử dụng cơ sở dữ liệu
    let orderData = {
        _id: mongoose.Types.ObjectId(),
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        orderDetail: body.orderDetail,
        cost: body.cost
    }
    orderModel.create(orderData, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: Internal Sever Error",
                message: err.message
            })
        } else {
            return res.status(201).json({
                status: "Success: Create Order Success",
                data: data
            })
        }
    })
}
// tạo get all  
const getAllOrder = (req, res) => {
    orderModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: Internal Sever Error",
                message: err.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get All Order Successfully",
                data: data
            })
        }
    })
}
// tạo get by id 
const getOrderById = (req, res) => {
    // lấy param 
    let orderId = req.params.orderId;
    //B2 : Validate
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "orderId is not valid"
        })
    }
    // B3: Thao tắc với cơ sở dữ liệu
    orderModel.findById(orderId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: Internal sever Error",
                message: err.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get Order by id success" + orderId,
                data: data
            })
        }
    })
}
// tạo post 
const updateOrder = (req, res) => {
    // thu thập dữ liệu
    let orderId = req.params.orderId;
    let body = req.body;
    // validate
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "orderId is not valid"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    let orderData = {
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        orderDetail: body.orderDetail,
        cost: body.cost
    }
    orderModel.findByIdAndUpdate(orderId, orderData, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: Internal sever error",
                message: err.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Update Order Success",
                data: data
            })
        }
    })
}
// tạo post 
const deleteOrder = (req, res) => {
    // B1: thu thập dữ liệu
    let orderId = req.params.orderId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "orderId is not valid"
        })
    }
    //B3: Thao tắc với cơ sở dữ liệu
    orderModel.findByIdAndDelete(orderId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: Internal sever error",
                message: err.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Delete Order success " + orderId,
            })
        }
    })
}

module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrder,
    deleteOrder
}

