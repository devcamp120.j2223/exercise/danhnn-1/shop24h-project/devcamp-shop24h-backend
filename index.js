const express = require('express');
const app = express();
const mongoose = require('mongoose');
const productTypeRouter = require('./App/Router/productTypeRouter')
const productRouter = require('./App/Router/productRouter')
const customerRouter = require('./App/Router/customerRouter')
const orderRouter = require('./App/Router/orderRouter')

// Kết nối với MongoDB:
mongoose.connect("mongodb://localhost:27017/Shop_24h", function(error) {
 if (error) throw error;
 console.log('Successfully connected');
})

const port = 8000;

app.use(express.json());

app.use(express.urlencoded({extends: true}))

app.use('/', productTypeRouter)
app.use('/', productRouter)
app.use('/', customerRouter)
app.use('/', orderRouter)


app.listen(port, () => {
    console.log(`app running on port ${port}`)
})